Etape 1 : Clermont-ferrand -> Le Puy-en-Velais
* Haut-Forez
* Mont de la madeleine
* Gorge de la Loire

Etape 2 : Le Puy-en-velais -> Mende
* Sources de la Loire 
* Gorge de l'allier (Langeac-Langone)

Etape 3 : Mende -> Laguiole
* Gorge du Lot
* Aubrac

Etape 4 : Laguiole -> Salers
* Plomb du Cantal
* Gorge de la truyère

Etape 5 : Salers -> Clermont-ferrand
* Gorge de la dordogne
* Sancy 